import React from 'react';
import PropTypes from 'prop-types';

import BrandLink from './BrandLink';
import Wrapper from './Wrapper';

function Brand(props) {
  return (
    <Wrapper>
      <BrandLink href={`brands/${props.item.id}`}>
        {props.item.label}
      </BrandLink>
    </Wrapper>
  );
}

Brand.propTypes = {
  item: PropTypes.object.isRequired,
};

export default Brand;
