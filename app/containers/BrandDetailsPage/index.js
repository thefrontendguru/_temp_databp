import React from 'react';
import PropTypes from 'prop-types';
import { Helmet } from 'react-helmet';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import injectSaga from 'utils/injectSaga';
import CenteredSection from 'components/CenteredSection';

import { load, reset } from './actions';
import { makeSelectLoading, makeSelectBrand, makeSelectCaseAmount } from './selectors';
import saga from './saga';

export class BrandDetailsPage extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function

  static propTypes = {
    onLoad: PropTypes.func.isRequired,
    onReset: PropTypes.func.isRequired,
    match: PropTypes.object.isRequired,
    brand: PropTypes.object.isRequired,
    loading: PropTypes.bool.isRequired,
  }

  componentWillMount() {
    const { onLoad, match: { params } } = this.props;
    if (params) onLoad(params);
  }

  componentWillUnmount() {
    this.props.onReset();
  }

  render() {
    const { loading, brand } = this.props;
    return loading || !brand.label ? <span>...loading</span> : this.renderPage();
  }

  renderPage = () => {
    const { brand } = this.props;
    return (
      <div>
        <Helmet>
          <title>{brand.label}</title>
          <meta name="description" content={brand.label} />
        </Helmet>
        <CenteredSection>
          {this.renderHeader()}
          {this.renderBody()}
        </CenteredSection>

      </div>
    );
  }

  renderHeader = () => {
    const { label } = this.props.brand;
    const caseAmount = this.renderCaseAmount();
    return (
      <div style={styles.brandDetailHeader}>
        <div style={styles.brandHeaderTitleCont}><h1 style={styles.brandHeaderTitle}>{label}</h1></div>
        <div style={styles.brandHeaderAmountCont}>
          <span style={styles.brandHeaderAmountInner}>
            <span style={styles.brandHeaderAmountText}>Total</span>
            <span style={styles.brandHeaderAmount}>{caseAmount}</span>
          </span>
        </div>
      </div>
    );
  }

  renderBody = () => (
    <table className="table">
      <thead>
        <tr>
          <th scope="col">Case Type</th>
          <th scope="col">Bottles </th>
          <th scope="col">Cans </th>
        </tr>
      </thead>
      <tbody>
        {
                    this.getItems().map((row) => (
                      <tr>
                        <th scope="row">{row.title}</th>
                        <td>{row.totalBottle}</td>
                        <td>{row.totalCan}</td>
                      </tr>
                        ))
                }
      </tbody>
    </table>
        )

  renderCaseAmount = () => {
    const { cases } = this.props.brand;
    const amount = cases.map((item) => item.amount);

    return amount.reduce((a, b) => a + b, 0);
  };

  renderTotalItems = (count, type) => {
    const { cases } = this.props.brand;
    const amount = cases.map((item) => {
      if (item.size === count && item.type === type) {
        return item.amount;
      }
      return null;
    });

    return amount.reduce((a, b) => a + b, 0);
  };

  getItems = () => [
    {
      title: '8 Count',
      totalBottle: this.renderTotalItems(8, 'bottle'),
      totalCan: this.renderTotalItems(8, 'can'),
    },
    {
      title: '12 Count',
      totalBottle: this.renderTotalItems(12, 'bottle'),
      totalCan: this.renderTotalItems(12, 'can'),
    },
    {
      title: '24 Count',
      totalBottle: this.renderTotalItems(24, 'bottle'),
      totalCan: this.renderTotalItems(24, 'can'),
    },

  ]
}

const styles = {
  brandDetailHeader: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginTop: 15,
    marginBottom: 15,
  },
  brandHeaderTitleCont: {
    flex: 1,
  },
  brandHeaderTitle: {
    textAlign: 'left',
    fontSize: 40,
  },
  brandHeaderAmountCont: {
    flex: 0,
    whiteSpace: 'nowrap',
  },
  brandHeaderAmountInner: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    display: 'flex',
    backgroundColor: '#e6e6e6',
    borderRadius: 40,
    border: '1px solid #ddd',
    padding: 5,
    paddingLeft: 20,
    paddingRight: 20,
    minWidth: 150,
  },
  brandHeaderAmountText: {
    textTransform: 'uppercase',
    fontSize: 16,
    color: '#8c8c8c',
  },
  brandHeaderAmount: {
    fontWeight: 'bold',
    fontSize: 24,
  },
};


export function mapDispatchToProps(dispatch) {
  return {
    onLoad: (params) => dispatch(load(params)),
    onReset: () => dispatch(reset()),
  };
}

const mapStateToProps = createStructuredSelector({
  loading: makeSelectLoading(),
  brand: makeSelectBrand(),
  caseAmount: makeSelectCaseAmount(),
});

const withConnect = connect(mapStateToProps, mapDispatchToProps);
const withSaga = injectSaga({ key: 'details', saga });

export default compose(
    withSaga,
    withConnect,
)(BrandDetailsPage);
