import React from 'react';
import H1 from 'components/H1';
import A from 'components/A';

export default function NotFound() {
  return (
    <article>
      <H1>You lost, Huh...</H1>
      <A href="/">Home </A>
    </article>
  );
}
