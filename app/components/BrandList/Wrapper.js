import styled from 'styled-components';

const Wrapper = styled.div`
  padding: 0;
  margin: 0;
  width: 100%;
  background-color: white;
  border: 1px solid #ccc;
  border-radius: 3px;
  overflow: hidden;
  box-shadow: 1px 1px 2px #ccc;
`;

export default Wrapper;
