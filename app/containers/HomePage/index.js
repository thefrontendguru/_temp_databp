import React from 'react';
import PropTypes from 'prop-types';
import { Helmet } from 'react-helmet';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';

import injectSaga from 'utils/injectSaga';

import { loadBrands } from 'containers/App/actions';

import {
  makeSelectBrands,
  makeSelectLoading,
} from 'containers/App/selectors';

import H3 from 'components/H3';
import BrandList from 'components/BrandList/';

import CenteredSection from 'components/CenteredSection';
import Section from 'components/Section';
import saga from './saga';

export class HomePage extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function

  componentWillMount() {
    this.props.onLoad();
  }

  render() {
    return (
      <article>
        <Helmet>
          <title>Brands</title>
          <meta name="description" content="Brands" />
        </Helmet>
        <div>
          <CenteredSection>
            <H3>Brands</H3>
            <p>Brands currently available in store</p>
          </CenteredSection>
          <Section>
            <BrandList {...this.props} />
          </Section>
        </div>
      </article>
    );
  }
}

HomePage.propTypes = {
  onLoad: PropTypes.func.isRequired,
};

export function mapDispatchToProps(dispatch) {
  return {
    onLoad: () => dispatch(loadBrands()),
  };
}


const mapStateToProps = createStructuredSelector({
  brands: makeSelectBrands(),
  loading: makeSelectLoading(),
});

const withConnect = connect(mapStateToProps, mapDispatchToProps);
const withSaga = injectSaga({ key: 'home', saga });

export default compose(
  withSaga,
  withConnect,
)(HomePage);
