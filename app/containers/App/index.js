import React from 'react';
import { Helmet } from 'react-helmet';
import AppWrapper from './AppWrapper';
import Routes from '../Routes';

export default function App() {
  return (
    <AppWrapper>
      <Helmet
        titleTemplate="Store Manager"
        defaultTitle="Store Manager"
      >
        <meta name="description" content="A pretty great application to manage a store" />
      </Helmet>
      <Routes />
    </AppWrapper>
  );
}
