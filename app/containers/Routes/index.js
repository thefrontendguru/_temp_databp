import React from 'react';
import { Switch, Route } from 'react-router-dom';
import HomePage from 'containers/HomePage/Loadable';
import BrandDetailsPage from 'containers/BrandDetailsPage/Loadable';
import NotFoundPage from 'containers/NotFoundPage/Loadable';

export class Routes extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <Switch>
        <Route exact path="/" component={HomePage} />
        <Route path="/brands/:brandId" component={BrandDetailsPage} />
        <Route path="" component={NotFoundPage} />
      </Switch>
    );
  }
}


export default Routes;
