# DataBp Front-End Homework Assignment

Thank you for applying with us and taking the time to do this homework. Please read this file carefully all the way to the end! We hope you enjoy the project.

## Scenario

You’re a fullstack engineer, working on an application that manages inventory for a beer store.
This happens to be a pretty cutting edge store that uses [some up-to-date best-of-breed Javascript libraries](https://www.reactboilerplate.com/) ([docs](https://github.com/react-boilerplate/react-boilerplate/tree/master/docs)), and you’re fortunate enough to be part of
an engineering team dedicated to seeing that this application continues to provide value.

## Application

Store manager users get an overview of what brands are currently available in store,
with the ability to dig into a specific brand to see how many cases are in stock.

## Assignment

Your team is often faced with product and engineering demands in parallel.
This assignment involves solving problems for the product team as well as
your own engineering team.

## Product Problem

The product team wants to be able to visually compare inventory amounts by case size (8, 12, 24) and type (bottles VS cans)
with the eventual goal of making more informed decisions when stocking the store.
They would also like to see the total case count displayed on the home page beside each brand name.
You can find the needed data in `app/assignment-data.json` and the design mocks in `mocks`.
(tobe same as BE req.)

## Engineering Problem

There are a number of tech debt items that can be fixed while addressing the product team demands.

These tech debt items are marked with TODO comments throughout the code base and include details for what needs to be done to fix them.

## Assignment Details

* Use git for all required work (beginning with a `git init`). The first commit should be a copy of the provided assignment. We'll look at your git history when you return your homework.
* Enhance the application to allow users to visually compare inventory amounts by case size and type.
* Address the tech debt TODO items in the codebase.
* Complete as much of the work as you can, at a quality level you'd be comfortable offering up as a PR for review, in 4 hours or less.
* We care about your Javascript first. Leave CSS, to the end, if you've done everything else you want.
* Please don't make this assignment or your work public. If you want to use Github, Gitlab, or Bitbucket, please put this code in a private repository.
* After your work is complete, return it to us.

## What we're looking for

* Javascript knowledge.
* Problem solving and time management.
* Your ability to address tech debt without getting derailed from your primary goals.
* Git and source control knowledge.
* Application state management.
* Ability to adapt to provided build tools.

## Get started

* Run `$ yarn` to install dependencies.
* Run `$ yarn start` store manager will be running on [`localhost:3000`](http://localhost:3000).
