import {
  LOAD,
  LOAD_SUCCESS,
  RESET,
} from './constants';

export function load(brandId) {
  return {
    type: LOAD,
    brandId,
  };
}

export function loaded(brand) {
  return {
    type: LOAD_SUCCESS,
    brand,
  };
}

export function reset() {
  return {
    type: RESET,
  };
}
