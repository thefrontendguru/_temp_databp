import React from 'react';
import { map } from 'lodash';

import Brand from 'components/Brand';

import Ul from './Ul';
import Wrapper from './Wrapper';

function BrandList(props) {
  return (
    <Wrapper>
      <Ul>
        {map(props.brands, (brand, key) =>
          <Brand key={`brand-${key}`} item={brand} />
        )}
      </Ul>
    </Wrapper>
  );
}

export default BrandList;
