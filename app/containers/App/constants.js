export const LOAD_BRANDS = 'store-manager/App/LOAD_BRANDS';
export const LOAD_BRANDS_SUCCESS = 'store-manager/App/LOAD_BRANDS_SUCCESS';
export const LOAD_BRANDS_ERROR = 'store-manager/App/LOAD_BRANDS_ERROR';
export const DEFAULT_LOCALE = 'en';
