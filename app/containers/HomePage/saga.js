import { put, takeLatest } from 'redux-saga/effects';
import { LOAD_BRANDS } from 'containers/App/constants';
import { brandsLoaded } from 'containers/App/actions';

import { brands } from 'assignment-data.json';

export function* getBrands() {
  yield put(brandsLoaded(brands));
}

export default function* brandData() {
  yield takeLatest(LOAD_BRANDS, getBrands);
}
