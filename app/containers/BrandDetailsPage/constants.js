export const LOAD = 'store-manager/BrandDetailsPage/LOAD';
export const LOAD_SUCCESS = 'store-manager/BrandDetailsPage/LOAD_SUCCESS';
export const SET_BRAND = 'store-manager/BrandDetailsPage/SET_BRAND';
export const RESET = 'store-manager/BrandDetailsPage/RESET';
