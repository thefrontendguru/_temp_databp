import { put, takeLatest } from 'redux-saga/effects';
import { brands } from 'assignment-data.json';

import { brandsLoaded } from 'containers/App/actions';

import { loaded } from './actions';
import { LOAD } from './constants';

export function* load(params) {
  yield put(brandsLoaded(brands));
  yield put(loaded(brands[params.brandId.brandId]));
}

export default function* brandDetailData() {
  yield takeLatest(LOAD, load);
}
